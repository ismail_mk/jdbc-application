package com.greatlearning.jdbcassignment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UserDAO {

	Connection connection = null;
	Statement stmt = null;

	// method to open a connection
	public void openConnection() throws SQLException {

		// Creating the connection
		connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb", "root", "admin");
		// Access a Statement
		stmt = connection.createStatement();
	}

	// method to close the connection	
	public void closeConnection() throws SQLException {
		stmt.close();
		connection.close();
	}

	// method to display the users
	public void displayUsers() throws SQLException {
		String str = "select userId,firstName,lastName,email from user";
		ResultSet rs = stmt.executeQuery(str);
		int rowCounter = 0;
		System.out.println("Users\n");
		while (rs.next()) {
			int userId = rs.getInt("userId");
			String firstName = rs.getString("firstName");
			String lastName = rs.getString("lastName");
			String email = rs.getString("email");

			System.out.println("userId = " + userId + ", firstName = " + firstName + ", lastName = " + lastName
					+ ", email = " + email);
			++rowCounter;
		}
		System.out.println("Number of Users: " + rowCounter);
	}

	// method to register the new user
	public void RegisterUser(int userid, String firstName, String lastName, String email) throws SQLException {
		if (connection != null && !connection.isClosed()) {
			String str = "insert into user (userId,firstName,lastName,email) values (?,?,?,?)";
			PreparedStatement preparedStatement = connection.prepareStatement(str);
			preparedStatement.setInt(1, userid);
			preparedStatement.setString(2, firstName);
			preparedStatement.setString(3, lastName);
			preparedStatement.setString(4, email);
			int ret = preparedStatement.executeUpdate();
			if (ret == 1) {
				System.out.println("Registration successful!!!");
			} else {
				System.out.println("Registration unsuccessful!!!");
			}

		}

	}

	// method to delete user
	public void deleteUser(int userid) throws SQLException {
		String sql = "delete from user where userId= " + userid;
		int counter = stmt.executeUpdate(sql);
		if (counter == 1) System.out.println(userid + " successfully deleted!!!");
	}

	// method to update user
	public void updateUser(int userid, String colName, String colValue) throws SQLException {
		String sql = "update user set "+ colName +" ='" + colValue  + "' where userId=" + userid;
				
		int ret = stmt.executeUpdate(sql);
		if (ret == 1) {
			System.out.println("Update successful!!!");
		} else {
			System.out.println("Update unsuccessful!!!");
		}
	}
}