package com.greatlearning.jdbcassignment;


import java.sql.SQLException;
import java.util.Scanner;

public class UserApp {
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		UserDAO userDAO = new UserDAO();
		try {
			int input;
			int userid;
			String firstName;
			String lastName;
			String email;
			
			do {
				System.out.println("!!!!  Welcome to User CRUD services  !!!!");
				System.out.println();
				System.out.println("1. Registration");
				System.out.println("2. Update");
				System.out.println("3. Display data");
				System.out.println("4. Delete");
				System.out.println("0. Exit");
				
				// open the connection
				userDAO.openConnection();
				
				Scanner scan = new Scanner(System.in);
				input = scan.nextInt();
				switch (input) {
				case 1:
					Scanner scan1 = new Scanner(System.in);
					System.out.println("Enter the userId:");
					userid = scan1.nextInt();
					System.out.println("Enter the firstName:");
					firstName = scan1.next();
					System.out.println("Enter the lastName:");
					lastName = scan1.next();					
					System.out.println("Enter the email:");
					email = scan1.next();
					
					userDAO.RegisterUser(userid, firstName, lastName, email);
					break;
					
				case 2:
					Scanner scan2 = new Scanner(System.in);
					System.out.println("Enter the userId, you want to modify :");
					userid = scan2.nextInt();
					System.out.println("Select the field name to Modify :");
					System.out.println("1. firstNamee");
					System.out.println("2. lastName");
					System.out.println("3. email");
					int colNumber = scan2.nextInt();
					String colName=null;
					switch (colNumber) {
					case 1:
						colName = "firstName";
						break;
					case 2:
						colName = "lastName";
						break;
					case 3:
						colName = "email";
						break;
					default:
						break;
					}
					System.out.println("Enter the Value:");
					String colValue = scan2.next();					

					userDAO.updateUser(userid, colName, colValue);
					break;
					
				case 3:
					userDAO.displayUsers();
					break;
					
				case 4:
					Scanner scan3 = new Scanner(System.in);
					System.out.println("Enter the userId, you want to delete :");
					userid = scan3.nextInt();					
					userDAO.deleteUser(userid);
					break;

				default:
					break;
				}
			} while (input > 0);
			
			// close the connection
			userDAO.closeConnection();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}