CREATE TABLE `user` (
  `userId` int NOT NULL,
  `firstName` varchar(50) DEFAULT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='user table		';



INSERT INTO `mydb`.`user` (`userId`, `firstName`, `lastName`, `email`) VALUES ('100', 'Mohamed', 'Ismail', 'mohd@gmail.com');
INSERT INTO `mydb`.`user` (`userId`, `firstName`, `lastName`, `email`) VALUES ('101', 'krishna', 'Kumar', 'kris@gmail.com');
INSERT INTO `mydb`.`user` (`userId`, `firstName`, `lastName`, `email`) VALUES ('102', 'Venkat', 'Raman', 'venkat@gmail.com');
INSERT INTO `mydb`.`user` (`userId`, `firstName`, `lastName`, `email`) VALUES ('103', 'Mohamed', 'Imran', 'imr@yahoo.com');
